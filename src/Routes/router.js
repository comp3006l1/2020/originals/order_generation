import express from "express";

//controllers
import { stepLogger } from "../Controller/clockController";
import {
  getAllOrders,
  getselectedOrder,
  updateselectedOrder,
} from "../Controller/orderController";

//middleware
import validate from "../Middleware/validate";
import auth from "../Middleware/auth";

//validations
import { stepCount, statusUpdate } from "../Validations/clock.validations";

const router = express.Router();

router.put("/clock", validate(stepCount), stepLogger);
router.get("/orders", getAllOrders);
router.get("/orders/:id", getselectedOrder);
router.put("/orders/:id", validate(statusUpdate), updateselectedOrder);

export default router;
