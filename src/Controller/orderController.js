import httpStatus from "http-status";

//utils
import catchAsync from "../Utils/CatchAsync";

//service
import {
  fetchAllOrders,
  fetchOrdersByID,
  updateOrdersByID,
} from "../Service/order.service";

export const getAllOrders = catchAsync(async (req, res) => {
  const allOrders = await fetchAllOrders();

  const response = {
    success: allOrders ? true : false,
    orders: allOrders.map((data) => {
      let newOrder = {
        id: data._id,
        ...data,
      };

      delete newOrder._id;
      return newOrder;
    }),
  };
  res.status(httpStatus.OK).send(response);
});

export const getselectedOrder = catchAsync(async (req, res) => {
  let allOrders = await fetchOrdersByID(req.params.id);

  allOrders = {
    id: allOrders._id,
    ...allOrders,
  };

  delete allOrders._id;

  const response = {
    success: allOrders ? true : false,
    orders: allOrders,
  };
  res.status(httpStatus.OK).send(response);
});

export const updateselectedOrder = catchAsync(async (req, res) => {
  let updatedOrder = await updateOrdersByID(req.params.id, req.body.status);

  const response = {
    success: updatedOrder ? true : false,
  };
  res.status(httpStatus.OK).send(response);
});
