import httpStatus from "http-status";

//utils
import catchAsync from "../Utils/CatchAsync";

//service
import { geneareteOrders } from "../Service/order.service";

export const stepLogger = catchAsync(async (req, res) => {
  console.log("step counter working", req.body);

  geneareteOrders();

  const response = { success: true };
  res.status(httpStatus.CREATED).send(response);
});
