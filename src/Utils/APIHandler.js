import http from "https";
import axios from "axios";

export const sendGetRequest = async ({
  url = "",
  token = "",
  body = {},
  headers = {},
}) => {
  let ApiResponse = "";
  await axios
    .get(`${url}`, {
      params: {
        ...body,
      },
      headers: {
        "Content-Type": "application/json",
        "x-api-key": token,
        ...headers,
      },
    })
    .then((response) => {
      // console.log(response.data);
      // console.log(response);

      ApiResponse = response.data;
    })
    .catch((error) => {
      // console.log(error);
    });

  return ApiResponse;
};

export const sendPostRequest = async ({
  url = "",
  token = "",
  body = {},
  headers = {},
}) => {
  let ApiResponse = "";

  try {
    ApiResponse = await axios.post(`${url}`, body, {
      headers: {
        "Content-Type": "application/json",
        "x-api-key": token,
        ...headers,
      },
    });
  } catch (err) {
    ApiResponse = err;
  } finally {
    console.log("ApiResponse", ApiResponse.data);
    // console.log("ApiResponse", ApiResponse.status);
    // console.log("ApiResponse", ApiResponse);

    return ApiResponse;
  }
};
