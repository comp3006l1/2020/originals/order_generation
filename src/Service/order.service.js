import httpStatus from "http-status";
import bcrypt from "bcryptjs";
import moment from "moment";
import Probability from "probability-node";

import config from "../Config/config";

//utils
import AppError from "../Utils/AppError";

//models
import OrderModel from "../Models/Orders";

//itemList
import ItemList from "../../ItemList.json";

//services
import { getItemList } from "./item.service";

/**
 *Generate orders
 *
 * @returns orders
 */
export const geneareteOrders = async (req, res) => {
  console.log("geneareteOrders----------------");

  let probabilitilized = new Probability(
    {
      p: "20%" /* the probability by that ... */,
      f: function () {
        generateNewOrder();
      },
    },
    {
      p: "80%",
      f: function () {
        return;
      },
    }
  );

  probabilitilized();
};

const generateNewOrder = async () => {
  console.log("generateNewOrder----------------");

  let flag = false;

  let generateNewOrder = new Probability(
    {
      p: "50%",
      f: async () => {
        flag = true;
        await generateNewItemOrder(2);
      },
    },
    {
      p: "25%",
      f: async () => {
        flag = true;
        await generateNewItemOrder(3);
      },
    },
    {
      p: "13%",
      f: async () => {
        flag = true;
        await generateNewItemOrder(4);
      },
    },
    {
      p: "6%",
      f: async () => {
        flag = true;
        await generateNewItemOrder(5);
      },
    }
  );

  generateNewOrder();

  if (!flag) await generateNewItemOrder(1);
};

const generateNewItemOrder = async (itemCount) => {
  console.log("generateNewItemOrder----------------");

  let flag = false;

  let itemContent = new Probability(
    {
      p: "33%",
      f: async () => {
        flag = true;
        await generateNewItemQtyOrder(itemCount, 2);
      },
    },
    {
      p: "3%",
      f: async () => {
        flag = true;
        await generateNewItemQtyOrder(itemCount, 3);
      },
    }
  );

  itemContent();

  if (!flag) await generateNewItemQtyOrder(itemCount, 1);
};

/**
 * @param  {} itemCount
 * @param  {} itemQty
 */
const generateNewItemQtyOrder = async (itemCount, itemQty) => {
  console.log("items--", itemCount, itemQty);

  const itemListData = await getItemList();

  // Shuffle array
  const shuffled = itemListData.sort(() => 0.5 - Math.random());

  // Get sub-array of first n elements after shuffled
  let selected = shuffled.slice(0, itemCount);

  console.log("selected ----", selected);

  selected = selected.map((data) => ({
    ...data,
    quantity: itemQty,
  }));

  await addNewOrder(selected);
};

/**
 *
 * @param  {} orderDetails
 * @returns {} created order
 */
const addNewOrder = async (orderDetails) => {
  console.log("creating ----", orderDetails);
  let weight = 0;
  orderDetails.map((data) => (weight += data.weight));

  const createdOrder = await OrderModel.create({
    items: orderDetails,
    status: "NEW",
    weight,
  });

  return createdOrder;
};

/**
 *
 * @returns {} all orders
 */
export const fetchAllOrders = async () => {
  const orders = await OrderModel.find().lean();

  return orders;
};

/**
 *
 * @returns {} all orders
 */
export const fetchOrdersByID = async (id) => {
  const orders = await OrderModel.findById(id).lean();

  return orders;
};

/**
 *
 * @returns {} all orders
 */
export const updateOrdersByID = async (id, status) => {
  const orders = await OrderModel.findByIdAndUpdate(id, { status });

  return orders;
};
