import httpStatus from "http-status";
import bcrypt from "bcryptjs";
import moment from "moment";

import config from "../Config/config";

//utils
import AppError from "../Utils/AppError";
import { sendGetRequest } from "../Utils/APIHandler";

//models
import OrderModel from "../Models/Orders";

//itemList
import ItemList from "../../ItemList.json";

/**
 *Get Item List
 *
 * @returns item list
 */
export const getItemList = async (req, res) => {
  const API_ENDPOINT = config.simulator + "/items";

  const apiRespose = await sendGetRequest({
    url: API_ENDPOINT,
  });

  return apiRespose;
};
