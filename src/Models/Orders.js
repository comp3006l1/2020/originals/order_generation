import mongoose from "mongoose";
import { omit, pick } from "lodash";
// import bcrypt from "bcrypt";

const Schema = mongoose.Schema;

const itemSchema = new Schema(
  {
    id: String,
    name: String,
    weight: Number,
    supplier: String,
    location: String,
    quantity: Number,
  },
  { _id: false }
);

// Create Schema
const ordersSchema = new Schema(
  {
    status: {
      type: String,
      enum: ["NEW", "PROCESSING", "COMPLETED"],
      required: true,
    },
    items: [itemSchema],
    weight: Number,
  },
  {
    timestamps: true,
  }
);

ordersSchema.methods.transform = function () {
  let file = this;
  //   file.populate("uploadedBy");

  //   file.uploadedBy = file.uploadedBy
  //     ? file.uploadedBy.transform()
  //     : file.uploadedBy;
  //   file.files = file.files.map((data) => data.transform());

  return pick(file.toJSON(), ["_id", "status", "items", "createdAt"]);
};

const order = mongoose.model("orders", ordersSchema);

export default order;
