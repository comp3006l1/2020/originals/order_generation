import Joi from "@hapi/joi";

export const stepCount = {
  body: Joi.object().keys({
    step: Joi.number().required(),
  }),
};

export const statusUpdate = {
  body: Joi.object().keys({
    status: Joi.string().required(),
  }),
};
